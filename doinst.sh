#!/bin/bash
# warsaw slackware package (C) Ruben Carlo Benante <rcb@beco.cc> 2022-11-16

config() {
  NEW="$1"
  OLD="$(dirname $NEW)/$(basename $NEW .new)"
  # If there's no config file by that name, mv it over:
  if [ ! -r $OLD ]; then
    mv $NEW $OLD
  elif [ "$(cat $OLD | md5sum)" = "$(cat $NEW | md5sum)" ]; then
    # toss the redundant copy
    rm $NEW
  fi
  # Otherwise, we leave the .new copy for the admin to consider...
}

preserve_perms() {
  NEW="$1"
  OLD="$(dirname $NEW)/$(basename $NEW .new)"
  if [ -e $OLD ]; then
    cp -a $OLD ${NEW}.incoming
    cat $NEW > ${NEW}.incoming
    mv ${NEW}.incoming $NEW
  fi
  config $NEW
}

preserve_perms etc/rc.d/rc.warsaw.new
preserve_perms etc/warsaw.conf.new

if ! grep -qs rc.warsaw etc/rc.d/rc.local ; then
cat >> etc/rc.d/rc.local << EOF

# [warsaw-cut-tag-do-not-remove-begin-HASHz80]
# Warsaw v2.21.3 Slackware Package by Dr. Beco, 2022-11-15
#
# To enable warsaw sec mod to start at boot on Slackware
# you must chmod +x /etc/rc.d/rc.warsaw
#
if [ -x /etc/rc.d/rc.warsaw ]; then
  echo "Enabling warsaw security module"
  /etc/rc.d/rc.warsaw start
fi
# [warsaw-cut-tag-do-not-remove-end-HASHz80]

EOF
fi

