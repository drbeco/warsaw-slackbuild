#!/bin/bash
#
# Slackware build script for warsaw internet banking security module
# Version 2.21.3.20221116
#
# Copyright (C) 2022 Ruben Carlo Benante 
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

cd $(dirname $0) ; CWD=$(pwd)

PRGNAM=warsaw
VERSION=${VERSION:-2.21.3}
BUILD=${BUILD:-20221116}
TAG=${TAG:-_SBo}
PKGTYPE=${PKGTYPE:-txz}
DEBSRC=$(basename $(grep "DOWNLOAD_x86_64" $PRGNAM.info | cut -d"\"" -f2))
DEBDATA="data.tar.xz"
DEBDATATAR="-xvJ"

TMP=${TMP:-/tmp/SBo}
OUTPUT=${OUTPUT:-/tmp}
PKG=$TMP/package-$PRGNAM

if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) ARCH=i586 ;;
    arm*) ARCH=arm ;;
       *) ARCH=$( uname -m ) ;;
  esac
fi

# If the variable PRINT_PACKAGE_NAME is set, then this script will report what
# the name of the created package would be, and then exit. This information
# could be useful to other scripts.
if [ ! -z "${PRINT_PACKAGE_NAME}" ]; then
  echo "$OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE"
  exit 0
fi

#TODO support 32bits
if [ "$ARCH" != "x86_64" ]; then
  echo "$ARCH currently not supported."
  exit 1
fi

# exit on fail
set -e

# remove old directory
rm -rf $PKG
mkdir -p $TMP $PKG $OUTPUT

# ------------------ changing directory to package
(
    cd $PKG 

    # If correctly downloaded from the website 
    if [ -f $CWD/${DEBSRC} ]; then
        ar p $CWD/${DEBSRC} ${DEBDATA} | tar ${DEBDATATAR} 
        VERSION=$(ar p $CWD/${DEBSRC} control.tar.gz | tar -Ozxf - ./control | awk '/^Version:/{print $NF}')
    fi

    echo
    echo building VERSION $VERSION-$BUILD
    chown -R root:root .
    find -L . \
        \( -perm 777 -o -perm 775 -o -perm 750 -o -perm 711 -o -perm 555 -o -perm 511 \) \
        -exec chmod 755 {} \; -o \
        \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
        -exec chmod 644 {} \;
)

# ------------------ changing directory back to working dir $CWD
mkdir -p $PKG/etc/rc.d
mkdir -p $PKG/usr/doc/$PRGNAM

mv $PKG/usr/share/doc/$PRGNAM/ $PKG/usr/doc/
mv $PKG/usr/local/share/fonts/truetype/ $PKG/usr/local/share/fonts/TTF/
mv $PKG/etc/init/warsaw.conf $PKG/etc/warsaw.conf.new
mv $PKG/etc/init.d/warsaw $PKG/etc/rc.d/rc.warsaw.new

rm -rf $PKG/usr/share/doc/
rm -rf $PKG/etc/init/
rm -rf $PKG/etc/init.d/
rm -rf $PKG/lib/

cp $CWD/slack-desc $PKG/usr/doc/$PRGNAM/
cp $CWD/README $PKG/usr/doc/$PRGNAM/
cp $CWD/$PRGNAM.SlackBuild $PKG/usr/doc/$PRGNAM/

# wget -q -nc 'https://www.gnu.org/licenses/gpl-3.0.txt' -O $PKG/usr/doc/$PRGNAM/COPYING || true
cp $CWD/COPYING $PKG/usr/doc/$PRGNAM/

chmod -x $PKG/etc/rc.d/rc.warsaw.new
chmod -x $PKG/usr/doc/$PRGNAM/warsaw.SlackBuild

mkdir -p $PKG/install
cp $CWD/slack-desc $PKG/install/
cp $CWD/doinst.sh $PKG/install/doinst.sh
cp $CWD/douninst.sh $PKG/install/douninst.sh

mv $PKG/usr/doc/$PRGNAM/changelog.Debian.gz $PKG/usr/doc/$PRGNAM/changelog.Slackware.gz

# ------------------ changing directory to TMP to apply patch
(
    cd $TMP
    # manpage to include Slackware Usage information (gunzip, patch, gzip)
    zcat package-warsaw/usr/share/man/man1/warsaw.1.gz > package-warsaw/usr/share/man/man1/warsaw.1
    patch -p1 < $CWD/patches/warsaw-slackware-man1.patch
    gzip -f package-warsaw/usr/share/man/man1/warsaw.1
    # copyright file to include Slackware Package addendum
    patch -p1 < $CWD/patches/warsaw-slackware-copyright.patch
)

# ------------------ changing directory to package again
(
    cd $PKG
    /sbin/makepkg -l y -c y $OUTPUT/$PRGNAM-$VERSION-$ARCH-$BUILD$TAG.$PKGTYPE
)

exit 0

